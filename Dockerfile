FROM perl:5.26.1

RUN mkdir /tmp/build
WORKDIR /tmp/build

RUN mkdir hmmer && \
    curl http://eddylab.org/software/hmmer/hmmer-3.2.tar.gz | tar -zx -C hmmer --strip-components 1
RUN cd hmmer && ./configure && make && make install
RUN cd hmmer/easel && make install

RUN apt-get update && apt-get install -y libaprutil1-dev vim screen rsync scons libssh-dev less \
    aspell muscle mafft libgtk2.0-dev libglib2.0-dev libreadline6-dev && apt-get clean

# For SVN::Client need to point to serf library, so this needs to be built (with it's dependencies apr
# and openssl)
RUN wget https://www.openssl.org/source/openssl-1.0.2q.tar.gz && tar xf openssl-1.0.2q.tar.gz && \
    cd openssl-1.0.2q && ./config && make && make install
RUN wget http://mirror.vorboss.net/apache//apr/apr-1.6.5.tar.bz2 && tar xf apr-1.6.5.tar.bz2 && \
    cd apr-1.6.5/ && ./configure && make && make install
RUN wget https://www.apache.org/dist/serf/serf-1.3.9.tar.bz2 && tar xf serf-1.3.9.tar.bz2 && \
    cd serf-1.3.9 && scons APR=/usr/local/apr OPENSSL=/usr/local/ssl && scons install

# Dotter, Belvu and Blixem
RUN wget ftp://ftp.sanger.ac.uk/pub/resources/software/seqtools/PRODUCTION/seqtools-4.44.1.tar.gz && \
    tar xf seqtools-4.44.1.tar.gz && cd seqtools-4.44.1 && ./configure && make install

RUN cpan install -T Module::Build
RUN wget https://cpan.metacpan.org/authors/id/M/MS/MSCHWERN/Alien-SVN-v1.8.11.0.tar.gz && \
    tar xf Alien-SVN-v1.8.11.0.tar.gz && \
    cd Alien-SVN-v1.8.11.0 && PERL_MM_USE_DEFAULT=1 perl Build.PL && \
    cd src/subversion/ && ./configure --with-serf --with-apr=/usr/local/apr && \
    cd ../.. && ./Build && ./Build install

RUN cpan install -T Inline::MakeMaker Inline::C JSON Inline::MakeMaker Imager SVG Moose Inline::C \
    File::Rsync Data::UUID Config::General DBD::mysql Bio::Annotation::DBLink DBIx::Class Data::Printer \
    Lucy::Plan::Schema Data::Dump File::Slurp File::Temp XML::XPath Time::HiRes Text::Wrap SVN::Look \
    String::Diff Redis String::CRC32 Parallel::ForkManager Net::SCP DateTime::Format::MySQL \
    DBIx::Class::Result::Validation DBIx::Class::Result::ColumnData IPC::Run File::Touch Date::Object \
    Date::Calc Daemon::Control Data::Compare Term::ReadPassword Log::Log4perl

RUN git clone https://github.com/nawrockie/Bio-Easel.git && cd Bio-Easel && mkdir src && cd src && \
    git clone https://github.com/EddyRivasLab/easel.git easel && cd easel && git checkout tags/Bio-Easel-0.06 && \
    cd ../.. && perl Makefile.PL && make && make install

RUN git clone --depth 1 --branch master https://github.com/Janelia-Farm-Xfam/Consensus-Colors.git && \
    mv Consensus-Colors/lib/Consensus /opt

ENV PERL5LIB /opt:/opt/Pfam/PfamLib:/opt/Pfam/PfamSchemata
ENV LD_LIBRARY_PATH=/usr/local/lib
ENV PFAM_CONFIG /home/pfam/pfam.conf

RUN git clone --depth 1 --branch master https://github.com/Janelia-Farm-Xfam/Bio-HMM-Logo.git && \
    cd Bio-HMM-Logo && perl Makefile.PL && make && make install

WORKDIR /opt
RUN rm -rf /tmp/build

RUN git clone https://github.com/ProteinsWebTeam/Pfam.git

COPY files/symlinks .
RUN bash ./symlinks
RUN rm ./symlinks
RUN rm /usr/local/bin/efetch
COPY files/efetch /usr/local/bin/

RUN useradd -G staff -ms /bin/bash pfam
USER pfam
WORKDIR /home/pfam

RUN mkdir -p .subversion/auth/svn.ssl.server
COPY files/ssl-cert .subversion/auth/svn.ssl.server/3832aa3cb353866134a9a4cb11eb8903

CMD bash
