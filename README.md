PfamCuration container
======================

Summary
-------

This docker container provides the tools and environment for curation and building
of Pfam entries. The container may be built from this Dockerfile or is available as
an image (for x86_64) at dockerhub.ebi.ac.uk/pfam/pfam-curation. For full details see
the documentation at
[Read the Docs](https://pfam-curation-docs.readthedocs.io/en/latest).

Building the Docker container
-----------------------------

From inside the folder containing the `Dockerfile` file, run:

`docker build -t pfamcuration .`

Preparation
-----------

Prepare a Pfam config file, pfam.conf (or copy pfam.conf.example) and create a folder
to contain the working files, pfam_data. Using the bash script or otherwise, download
and unzip the files pfamseq.gz and uniprot.gz from the Pfam ftp site and place these
in a directory seqlib.

Running the container
---------------------

Run

`docker run --rm -it -v $(pwd)/pfam_data:/home/pfam/pfam_data -v $(pwd)/pfam.conf:/home/pfam/pfam.conf -v $(pwd)/seqlib:/data/seqlib pfamcuration`

